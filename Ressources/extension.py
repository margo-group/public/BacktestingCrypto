from zipline.data.bundles import register
from zipline.data.bundles.load_csv import load

eqSym = {
    "Bitcoin",
    "Ethereum",
}

register(
    'Blockchain',
    load(eqSym),
    calendar_name="NYSE"
)
